"""coaching URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from allauth.account.views import LoginView
from appprofile import views as appviews
from . import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', appviews.index ,name="index"),
    url(r'^api/get_schedule_web', appviews.get_schedules),
    url(r'^api/get_schedule', appviews.generate_schedule),
    url(r'^api/show_percent', appviews.show_percent),
    url(r'^api/start_time', appviews.start_time),
    url(r'^api/end_time', appviews.end_time),
    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^api/is_live', appviews.is_live),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^appprofile/', include('appprofile.urls')),
]


if settings.DEBUG:
    urlpatterns+=static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns+=static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)