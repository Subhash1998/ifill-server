from django.db import models
from device.models import Device

days=(
	(0,'Sunday'),
	(1,'Monday'),
	(2,'Tuesday'),
	(3,'Wednesday'),
	(4,'Thursday'),
	(5,'Friday'),
	(6,'Saturday')
	)

class Schedule(models.Model):
	id=models.AutoField(primary_key=True)
	start=models.TimeField()
	end=models.TimeField()
	day=models.IntegerField(default=0,choices=days)
	device=models.ForeignKey(Device,on_delete=models.CASCADE)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return str(str(self.device) + " => " + str(self.day))

	def as_dict(self):
		return {
			"id" : self.id,
			"start" : str(self.start),
			"end" : str(self.end),
			"day" : self.day
		}

	def overlap(self,start,end,day):
		existing= Schedule.objects.filter(day=day,start__lte=start,
			end__gte=end
    	)
		return not existing.exists() 
