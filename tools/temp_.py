import json
import time
import requests
import os
import serial
import sys
from multiprocessing import Process
from datetime import timedelta
import datetime
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)

RELAYS_1_GPIO=17

server = "http://192.168.43.54:8000"

payload = {
    "serial_id" : "4da7bddb-b26b-4422-aa97-809cb4ee5817",
    "serial_pk" : 1,
    "secret" : "b'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzZXJpYWxfaWQiOiI0ZGE3YmRkYi1iMjZiLTQ0MjItYWE5Ny04MDljYjRlZTU4MTciLCJzZXJpYWxfcGsiOiIxIn0.pFe95y2di4HoP0UiTzPqF1-9bcNJysQEgo8v8bqDZ3A'"
}

# def live_trigger():
while True:
    r = requests.get(server+'/api/is_live?id=1')
    new_schedule = json.loads(r.content.decode('UTF-8'))
    if(new_schedule["success"] == True):
        GPIO.output(RELAYS_1_GPIO,GPIO.HIGH)
    else:
        GPIO.output(RELAYS_1_GPIO,GPIO.LOW)
    time.sleep(2)
