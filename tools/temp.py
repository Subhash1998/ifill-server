import sys
import os
import termios, fcntl
import select
import RPi.GPIO as GPIO                    #Import GPIO library
import time

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

led_p = 16
led_n = 12

GPIO.setup(led_p,GPIO.OUT)
GPIO.setup(led_n,GPIO.OUT)

while(1):
    GPIO.output(led_p, 0)
    GPIO.output(led_n, 1)
    sleep(10)
    GPIO.output(led_p, 0)
    GPIO.output(led_n, 1)
    sleep(10)