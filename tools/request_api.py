import json
import time
import requests
import os
import serial
import sys
from multiprocessing import Process
from datetime import timedelta

server = "http://192.168.1.11:8000/api/get_schedule"

arduino_serial = {
    'distance': 0,
    'flow_meter_flag': False
}

schedule = {}

# def switch(x):
#     return {
#         0: 'Monday',
#         1: 'Tuesday',
#         2: 'Wednesday',
#         3: 'Thursday',
#         4: 'Friday',
#         5: 'Saturday',
#         6: 'Sunday'
#     }.get(x, 'Monday')  

def get_values_from_arduino():
    ser = serial.Serial('/dev/ttyACM0',9600)
    s = []
    while True:
        read_serial=ser.readline()
        str_msg = str(ser.readline())
        s = str_msg.split(':')
        arduino_serial['distance'] = int(s[0])
        arduino_serial['flow_meter_flag'] = int(s[1])
        print(arduino_serial)
        time.sleep(2)

def check_for_new_schedule():
    while True:
        new_schedule = {}
        cur_schedule = {}

        with open ('package.json') as package:
            data = json.load(package)

        payload = data
        r = requests.post(server, data=payload)
        schedule = json.loads(r.content.decode('UTF-8'))

        new_schedule = schedule['schedule']

        with open ('schedule_temp.json', 'w') as outfile:
            json.dump(schedule['schedule'], outfile)

        with open ('schedule.json') as package:
            cur_schedule = json.load(package)

        if cur_schedule == new_schedule:
            os.remove('schedule.json')
            os.rename('schedule_temp.json', 'schedule.json')

        schedule = cur_schedule
        print(cur_schedule)
        time.sleep(20)
    
def switch_motors():
    cur_time = str(timedelta(minutes=100))[:-3]
    now = datetime.datetime.now()
    day = now.strftime("%A")
    day = day.lower()
    schedules = schedule.day
    time.sleep(60)

def main():
    try:
        p1 = Process(target = get_values_from_arduino)
        p1.start()
        p2 = Process(target = check_for_new_schedule)
        p2.start()
    except:
        print ("Error: unable to start thread")

if __name__ == "__main__":
    main()
