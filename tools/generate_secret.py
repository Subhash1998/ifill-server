import jwt

encoded = jwt.encode({'serial_id' : "4da7bddb-b26b-4422-aa97-809cb4ee5817", 'serial_pk': '1'}, 'iFillSecret2k18iFillSecret2k18iFillSecret2k18', algorithm='HS256')
print(encoded)

decoded = jwt.decode(encoded, 'iFillSecret2k18iFillSecret2k18iFillSecret2k18', algorithms=['HS256'])
print(decoded)

