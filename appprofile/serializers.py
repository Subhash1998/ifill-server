from rest_framework import models
from rest_framework import serializers
from django.contrib.auth.models import User
from device.models import Device
from schedule.models import Schedule

class DeviceSerializer(serializers.ModelSerializer):

	class Meta:
		model = Device
		fields = '__all__'


class ScheduleSerializer(serializers.ModelSerializer):

	class Meta:
		model = Schedule
		fields = '__all__'

class DeviceStatus(serializers.ModelSerializer):

	class Meta:
		model = Device
		fields = ['status','serial_id']