from django.shortcuts import render,redirect
from rest_framework.authtoken.models import Token
from django.shortcuts import get_object_or_404
from django.http import JsonResponse,HttpResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from schedule.models import Schedule
from device.models import Device, Device_Registration
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.contrib.auth.decorators import login_required
from .serializers import DeviceSerializer,ScheduleSerializer,DeviceStatus
# from .forms import ScheduleForm
import jwt, json
import datetime
from datetime import timedelta
from django.contrib import messages
from django.urls import reverse
from rest_framework.authtoken.models import Token
import requests
from device.models import Cost

@csrf_exempt
def applogin(request):

	error_msg = {}
	error_msg['success'] = False
	if request.method=='POST':
		req_data = json.loads(request.body.decode('UTF-8'))
		email = req_data['username']
		password = req_data['password']
		try:
			obj = User.objects.filter(email=email)
			print(obj[0])
			username = obj[0].username
			print(username)
			user = authenticate(username=username, password=password)
			print(user)
		except User.DoesNotExist:
			error_msg['message'] = "Please create an account to continue"
			return HttpResponse('Unauthorized', status=401)
		except Exception as e:
			print(e)
			error_msg['message'] = "Please try again later"
			return HttpResponse('Unauthorized', status=401)
		
		if user:
			login(request,user)
			payload = {
				'id' : user.id,
				'email': user.email,
			}

			jwt_token = jwt.encode(payload,settings.SECRET_KEY)
			token = jwt_token.decode('utf-8')
			return JsonResponse({
				'success' : True,
				'message' : 'authentication successful',
				'first_name' : user.first_name,
				'last_name' : user.last_name,
				'token' : token
			})

		else:
			print("Invalid credentials")
			error_msg['message'] = "Invalid Credentials"
			return HttpResponse('Unauthorized', status=401)
	return HttpResponse("login")

@csrf_exempt
def appregister(request):

	if request.method == "POST":

		req_data = request.body.decode('UTF-8')
		req_data = json.loads(req_data)
		email = req_data['email']
		password = req_data['password']
		checkemail = req_data['email']
		if(User.objects.filter(email=checkemail).exists()):
			return JsonResponse({
				'success':False,
				'message':'email must be unique',
			})

		first = req_data['first_name']
		last = req_data['last_name']
		user = User.objects.create_user(
			username=first+last,
			first_name=first,
			last_name=last,
			email=email,
			password=password,
			is_active=False
			)
		user.save()
		payload = {
			'id' : user.id,
			'email': user.email,
		}

		jwt_token = jwt.encode(payload,settings.SECRET_KEY)
		token = jwt_token.decode('utf-8')
		return JsonResponse({
			'success' : True,
			'message' : 'Registration successful',
			'token' : token
		})
	else:
		return JsonResponse({
				'success' :False,
				'message' : 'form method error',
			})


@login_required
def profile(request):
	# device=Device.objects.filter(registered=False)
	registered=Device_Registration.objects.filter(user=request.user)
	device = Device.objects.filter(pk=registered[0].device.pk)
	amount=0
	start="00:00:00"
	end="00:00:00"
	time=timedelta(hours=0,minutes=0,seconds=0)
	for r in registered:
		try:
			print(r.device)
			cost=Cost.objects.filter(device=r.device)
			for c in cost:
				start=c.start
				end=c.end
				time1 = datetime.datetime.strptime(str(start)[:-7],'%H:%M:%S')
				time2 = datetime.datetime.strptime(str(end)[:-7],'%H:%M:%S')
				time += time2-time1
			days, seconds = time.days, time.seconds
			hours = days * 24 + seconds // 3600
			minutes = (seconds % 3600) // 60
			seconds = (seconds % 60)
			price=int(hours)*(18)+int(minutes)*(18)/60 + int(seconds)*(18)/3600
			print(price)
		except Exception as e:
			print(e)
	schedules=[]
	notify=0
	device_name=[]
	for d in registered:
		schedule=Schedule.objects.filter(device=d.device)
		for s in schedule:
			schedules.append(s)
	form=ScheduleForm()
	context={
		'user':request.user,
		'device':device,
		'registered':registered,
		'form':form,
		'schedules':schedules,
		'notify':notify,
		'device_name':device_name,
		'time':time,
		'start':start,
		'end':end,
		'price': price
	}
	return render(request,'appprofile/dashboard.html',context)

def index(request):
	return render(request, 'landing/new_index.html')

@csrf_exempt
def generate_schedule(request):
	response = {}
	temp_package = {}
	temp_package['serial_id'] = request.POST.get('serial_id')
	temp_package['serial_pk'] = request.POST.get('serial_pk')
	req_token = request.POST.get('secret')
	gen_secret = jwt.encode(temp_package, 'iFillSecret2k18iFillSecret2k18iFillSecret2k18', algorithm='HS256')
	print(req_token)
	print("gen_secret:")
	print(gen_secret.decode('UTF-8'))
	# if str(gen_secret) == str(req_token):
	print("User verified")
	device = Device.objects.get(pk = temp_package['serial_pk'])
	# for schedule in Schedule.objects.filter(device=device):
	# 	print(schedule)
	monday_s = [ obj.as_dict() for obj in Schedule.objects.filter(device=device, day = 1) ]
	tuesday_s = [ obj.as_dict() for obj in Schedule.objects.filter(device=device, day = 2) ]
	wednesday_s = [ obj.as_dict() for obj in Schedule.objects.filter(device=device, day = 3) ]
	thursday_s = [ obj.as_dict() for obj in Schedule.objects.filter(device=device, day = 4) ]
	friday_s = [ obj.as_dict() for obj in Schedule.objects.filter(device=device, day = 5) ]
	saturday_s = [ obj.as_dict() for obj in Schedule.objects.filter(device=device, day = 6) ]
	sunday_s = [ obj.as_dict() for obj in Schedule.objects.filter(device=device, day = 0) ]
	schedule = {
		"monday" : monday_s,
		"tuesday" : tuesday_s,
		"wednesday" : wednesday_s,
		"thursday" : thursday_s,
		"friday" : friday_s,
		"saturday" : saturday_s,
		"sunday" : sunday_s,
	}
	response['success'] = True
	response['schedule'] = schedule
	# else:
	# 	response['success'] = False
	# 	response['message'] = "Verification failed"
	print(response)
	return JsonResponse(response)

@csrf_exempt
def show_percent(request):

	response = {}
	response["success"]=False
	response["message"]="Device Verification Failed"
	temp_package = {}
	try:
		device = Device.objects.get(pk = 1)
		percentage = request.GET.get("percentage")
		device.percentage=int(percentage)
		device.save()

		response["success"]=True
		response["message"]="Percentage updated."
	except:
		success["message"]="Error updating tank percentage."

	print(response)

	return JsonResponse(response)


@login_required
def device_register(request):
	if request.method=='POST':
		device_id=request.POST.get('device_id')
		name=request.POST.get('name')
		data={}
		data["success"]=False
		try:
			device=Device.objects.filter(serial_id=device_id)
			if device.exists():
				device=Device.objects.get(serial_id=device_id)
				user=request.user
				if(Device_Registration.objects.filter(user=user,device=device).exists()): 
					data["msg"]="Device already registered"
					return JsonResponse(data)
				register=Device_Registration()
				register.user=user
				register.device=device
				register.save()

				device.registered=1
				device.name=name
				device.save()

				data["success"]=True
				data["msg"]="Device Registered Successfully"
			else:
				data["msg"]="Device currently unavailable"
				return JsonResponse(data)

		except Exception as e:
			print(e)
			data["msg"]="Error registering device.Try again later."

		return JsonResponse(data)


class DeviceList(APIView):

	def get(self,request):
		queryset = Device.objects.all()
		device = DeviceSerializer(queryset,many=True)
		return Response(device.data)

	def post(self,request):
		pass

def addschedule(request,*args,**kwargs):
	data={}
	data["success"]=False
	if request.method=='POST':
		try:
			r_id=request.POST.get('r_id')
			print(r_id)
			start=request.POST.get('start')	
			end=request.POST.get('end')
			day=request.POST.get('day')
			time1 = datetime.datetime.strptime(str(start),'%H:%M')
			time2 = datetime.datetime.strptime(str(end),'%H:%M')
			print(time1)
			if datetime.datetime.strptime(str(time2-time1),'%H:%M:%S') > datetime.datetime.strptime("00:00:00","%H:%M:%S"):

				obj=Schedule()
				if(obj.overlap(start,end,day)):
					if((Device_Registration.objects.filter(id=r_id)).exists()):
						device=Device_Registration.objects.get(id=r_id)
						print(device.device.name)
						obj.start=start
						obj.end=end
						obj.device=device.device
						obj.day=day
						obj.save()
						data["success"]=True
						data["msg"]="Schedule added for the device."
					else:
						data["msg"]="Device not registered."
				else:
					data["msg"]="Schedule overlaps with some other device."
			else:
				data["msg"]="Start time should be less than end time."

		except Exception as e:
			print(e)
			data["msg"]="Error adding schedule."
		print(data)
		return JsonResponse(data)


class ScheduleList(APIView):

	def get(self,request):
		queryset = Schedule.objects.all()
		schedule = ScheduleSerializer(queryset,many=True)
		return Response(schedule.data)

	def post(self,request):
		data={}
		data["success"]=False
		req_data=json.loads(request.body.decode('UTF-8'))
		print(req_data)
		r_id=req_data["r_id"]
		start=req_data["start"]
		end=req_data["end"]
		day=req_data["day"]
		try:
			obj=Schedule()
			if(obj.overlap(start,end,day)):
				if((Device_Registration.objects.filter(id=r_id)).exists()):
					device=Device_Registration.objects.get(id=r_id)
					obj.start=start
					obj.end=end
					obj.device=device.device
					obj.day=day
					obj.save()

					data["success"]=True
					data["msg"]="Schedule added for the device."
				else:
					data["msg"]="Device not registered."
			else:
				data["msg"]="Schedule overlaps with some other device."

		except Exception as e:
			print(e)
			data["msg"]="Error adding schedule."

		return JsonResponse(data)

@login_required
def schedule(request):
	device=Device_Registration.objects.filter(user=request.user)
	schedules=[]
	schedule=[]
	for d in device:
		try:
			s=Schedule.objects.filter(device=d.device)
			if s :
				schedules.append(s)
		except Exception as e:
			print(e)
	print(schedules)

	for sch in schedules:
		for s in sch:
			schedule.append(s)
	print(schedule)
	context={
		'schedule':schedule
	}
	return render(request,"appprofile/schedule.html",context)

def delete_schedule(request,s_id):
	try:
		schedule=Schedule.objects.get(id=s_id)
		schedule.delete()
		messages.success(request,"Device schedule deleted.")
	except Exception as e:
		print(e)
	return redirect("/appprofile/profile/")


class Errors(APIView):

	def post(self,request):
		data={}
		data["success"]=False
		req_data=json.loads(request.body.decode('UTF-8'))
		name=req_data["name"]
		data["msg"]=name
		return JsonResponse(data,safe=False)


def switch_motor(request):
	data={}
	data["success"]=False 
	data["on"]=0
	try:
		s_id=request.GET.get('s_id')
		schedule=Schedule.objects.get(id=s_id)
		print(schedule.device)
		message = ""
		try:
			on=0
			device=Device.objects.get(id=schedule.device.id)	
			if device.status == 0:
				device.status=1	
				device.save()
				on=1
				mesage = "Your device has been switched on"
			else:
				device.status=0
				mesage = "Your device has been switched off"
				device.save()
				on=0
			context={
				'schedule':schedule,
				'on':on
			}
			print(device.status)
			data["on"]=on
			data["msg"]="Motor toggled"
			r = requests.get('http://www.merasandesh.com/api/sendsms?username=E_SUMMIT&password=Summit125@&senderid=SUPORT&message=+'+message+'+&numbers=9407608477&unicode=0')
			return JsonResponse(data)	
		except Exception as e:
			print(e)
			data["msg"]="Device not scduled."
	except:
		data["msg"]="Device not scheduled."
	return JsonResponse(data)


def customise(request,s_id):
	schedule=Schedule.objects.get(id=s_id)
	device=Device.objects.get(id=schedule.device.id)
	on=0
	if device.status==1:
		on=1	
	context={
		'schedule':schedule,
		'on':on
	}
	return render(request,"appprofile/schedule.html",context)

class DeviceStats(APIView):

	def get(self,request):
		device=Device.objects.all()
		device=DeviceStatus(device,many=True)
		return Response(device.data)	

def device_list(request):
	try:
		token = request.META['HTTP_AUTHORIZATION']
		data = jwt.decode(token,settings.SECRET_KEY)
		user = User.objects.get(pk=data['id'])
		devices = [ obj.as_dict() for obj in Device_Registration.objects.filter(user=user) ]
		length = len(devices)
		response = {}
		response["success"] = True
		response["devices"] = devices
		response["length"] = length
		print (response)
		return JsonResponse(response)
	except Exception as e:
		print (e)
		return HttpResponse('Unauthorized', status=401)

def live_devices(request):
	try:
		token = request.META['HTTP_AUTHORIZATION']
		data = jwt.decode(token,settings.SECRET_KEY)
		user = User.objects.get(pk=data['id'])
		count = 0;
		devices = [ obj.as_dict() for obj in Device_Registration.objects.filter(user=user) ]
		for device in devices:
			if device["status"] == 1:
				count += 1
		response = {}
		response["success"] = True
		response["live"] = count
		print (response)
		return JsonResponse(response)
	except Exception as e:
		print (e)
		return HttpResponse('Unauthorized', status=401)

@csrf_exempt
def request_toggle(request):
	try:
		response = {}
		req_data = json.loads(request.body.decode('UTF-8'))
		device_r = Device_Registration.objects.get(pk=req_data["deviceId"])
		device = Device.objects.get(pk=device_r.device.pk)
		message = ""
		if device.status == 1:
			device.status = 0
			device.save()
			message = "Your device has been switched off"

		elif(device.percentage < 95):
			device.status = 1
			device.save()
			response['success'] = True
			message = "Your device has been switched on"
		response["success"] = False
		r = requests.get('http://www.merasandesh.com/api/sendsms?username=E_SUMMIT&password=Summit125@&senderid=SUPORT&message=+'+message+'+&numbers=9407608477&unicode=0')
		return JsonResponse(response)
	except Exception as e:
		print (e)
		return HttpResponse('Unauthorized', status=401)

def is_live(request):
	response = {}
	id = request.GET.get('id')
	device = Device.objects.get(pk=id)
	if device.status == 1:
		response["success"] = True
	else:
		response["success"] = False
	return JsonResponse(response)

@csrf_exempt
def get_schedules(request):
	try:
		token = request.META['HTTP_AUTHORIZATION']
		data = jwt.decode(token,settings.SECRET_KEY)
		user = User.objects.get(pk=data['id'])
		print(user)
		devices = Device_Registration.objects.filter(user=user)
		schedules = []
		count = 0
		for device_reg in devices:
			schedule = []
			temp = {}
			device = Device.objects.get(pk=device_reg.device.pk)
			schedule = [ obj.as_dict() for obj in Schedule.objects.filter(device=device) ]
			count += len(schedule)
			temp["device"] = device.as_dict()
			temp["schedule"] = schedule
			schedules.append(temp)
		response = {}
		response["success"] = True
		response["schedule"] = schedules
		response["length"] = count
		print (response)
		return JsonResponse(response)
	except Exception as e:
		print (e)
		return HttpResponse('Unauthorized', status=401)

@csrf_exempt
def start_time(request):
	response = {}
	response["success"]=False
	temp_package = {}
	temp_package['serial_id'] = request.POST.get('serial_id')
	temp_package['serial_pk'] = request.POST.get('serial_pk')
	temp_package['start_time'] = request.POST.get('start_time')
	req_token = request.POST.get('secret')
	gen_secret = jwt.encode(temp_package, 'iFillSecret2k18iFillSecret2k18iFillSecret2k18', algorithm='HS256')
	# if str(gen_secret) == str(req_token):
	try:
		device = Device.objects.get(pk = temp_package['serial_pk'])
		cost=Cost()
		cost.device=device
		device.status = 1
		device.save()
		# cost.start=temp_package["start_time"]
		cost.start=str(temp_package["start_time"])
		cost.save()
		mesage = "Your device has been switched on"
		r = requests.get('http://www.merasandesh.com/api/sendsms?username=E_SUMMIT&password=Summit125@&senderid=SUPORT&message=+'+message+'+&numbers=9407608477&unicode=0')

		response["success"]=True
		response["message"]="Start time updated."
	except Exception as e:
		print(e)
		response["message"]="Error updating start time."

	print(response)
	return JsonResponse(response)

	
@csrf_exempt
def end_time(request):
	response = {}
	response["success"]=False
	temp_package = {}
	temp_package['serial_id'] = request.POST.get('serial_id')
	temp_package['serial_pk'] = request.POST.get('serial_pk')
	temp_package['end_time'] = request.POST.get('end_time')
	req_token = request.POST.get('secret')
	gen_secret = jwt.encode(temp_package, 'iFillSecret2k18iFillSecret2k18iFillSecret2k18', algorithm='HS256')

	# if str(gen_secret) == str(req_token):
	try:
		device = Device.objects.get(pk = temp_package['serial_pk'])
		cost=Cost.objects.filter(device=device).order_by('updated_at')
		cost = cost[0]
		# cost.start=temp_package["end_time"]
		cost.end=str(temp_package["end_time"])
		cost.save()
		device.status=0
		device.save()

		response["success"]=True
		response["message"]="End time updated."
		mesage = "Your device has been switched off"
		r = requests.get('http://www.merasandesh.com/api/sendsms?username=E_SUMMIT&password=Summit125@&senderid=SUPORT&message=+'+message+'+&numbers=9407608477&unicode=0')
	except:
		response["message"]="Error updating end time."

	print(response)

	return JsonResponse(response)
	
