from django.db import models
from django.contrib.auth.models import User

status_choice=(
	(0,'OFF')	,
	(1,'ON')
	)

reg_choice=(
	(0,'NO')	,
	(1,'YES')
	)

class Device(models.Model):

	id=models.AutoField(primary_key=True)
	serial_id=models.CharField(max_length=100)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	name=models.CharField(max_length=100,null=True,blank=True)
	registered=models.IntegerField(default=0,choices=reg_choice)
	status=models.IntegerField(default=0,choices=status_choice)
	percentage = models.IntegerField(default=0, null=True, blank=True)
	
	def __str__(self):
		return str(self.name)

	def as_dict(self):
		return {
			"id" : self.id,
			"name" : str(self.name),
			"serial_id" : str(self.serial_id),
			"status" : self.status,
			"registered" : self.registered,
			"percentage": self.percentage
		}


class Device_Registration(models.Model):

	id=models.AutoField(primary_key=True)
	user=models.ForeignKey(User,on_delete=models.CASCADE)
	device=models.ForeignKey(Device,on_delete=models.CASCADE)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.user.username + ' ' + self.device.serial_id

	def as_dict(self):
		return {
			"id" : self.id,
			"name" : str(self.device.name),
			"serial_id" : str(self.device.serial_id),
			"status" : self.device.status,
			"registered" : self.device.registered,
			"percentage": self.device.percentage
		}

class Cost(models.Model):

	id=models.AutoField(primary_key=True)
	start=models.TimeField(null=True,blank=True)
	end=models.TimeField(null=True,blank=True)
	device=models.ForeignKey(Device,on_delete=models.CASCADE)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return str(self.device.name)