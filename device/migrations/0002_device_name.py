# Generated by Django 2.0.5 on 2018-10-04 05:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('device', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='device',
            name='name',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
